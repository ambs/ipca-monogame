
using System;
using Microsoft.Xna.Framework;

namespace IPCA.Monogame
{
    public class Camera {

        public static Camera Instance => _instance;

        private static Camera _instance; 
        private Vector2 _pxSize; // Monogame window size
        private Vector2 _wdSize; // Monogame window in worldsize
        private Vector2 _ratio;
        private Vector2 _target;

        public Camera(Game game, Vector2 worldSize)
        {
            _pxSize = new Vector2(game.GraphicsDevice.Viewport.Width, game.GraphicsDevice.Viewport.Height);
            _auxConstructor(game, worldSize);
        }

        public Camera(Game game, float worldWidth = 0f, float worldHeight = 0f) {
            _pxSize = new Vector2(game.GraphicsDevice.Viewport.Width, game.GraphicsDevice.Viewport.Height);
          
            if (worldWidth == 0 && worldHeight == 0)
                throw new Exception("Camera called with zero dimentions");

            if (worldWidth == 0)
                worldWidth = _pxSize.X * worldHeight / _pxSize.Y;
            if (worldHeight == 0)
                worldHeight = _pxSize.Y * worldWidth / _pxSize.X;
            _auxConstructor(game, new Vector2(worldWidth, worldHeight));
        }

        public void _auxConstructor(Game game, Vector2 worldSize) {
            if (_instance != null)
                throw new Exception("Singleton called twice");
            _instance = this;

            _target = Vector2.Zero;
            _wdSize = worldSize;
            
            _updateRatio();
        }

        private void _updateRatio() { _ratio = _pxSize / _wdSize; }

        public static void Zoom(float zoom) {
            _instance._wdSize *= zoom;
            _instance._updateRatio();
        }

        public static Vector2 ToPixel(Vector2 pos)
        {
            return new Vector2((pos.X - _instance._target.X + _instance._wdSize.X / 2f) * _instance._ratio.X,
                                _instance._pxSize.Y - (pos.Y - _instance._target.Y + _instance._wdSize.Y / 2f) * _instance._ratio.Y);
        }

        public static Vector2 ToLength(Vector2 len)
        {
            return len * _instance._ratio;
        }

        public static void LookAt(Vector2 tgt) {
            _instance._target = tgt;
        }

    }
}