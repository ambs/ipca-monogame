using System;
using IPCA.PixelDrawing;
using Microsoft.Xna.Framework;

namespace IPCA.Collision
{
    public abstract class Collider
    {
        protected Game _game;
        protected bool _debug;
        internal Vector2 _position;
        internal bool _inCollision;
        protected Collider(Game game)
        {
            _debug = false;
            _game = game;

            try { var _ = new Pixel(game); } catch (Exception) { }
        }

        public void SetDebug(bool flag)
        {
            _debug = flag;
        }

        public virtual void SetPosition(Vector2 position)
        {
            _position = position;
        }

        public abstract void Draw(GameTime gameTime);
        public abstract void Rotate(float theta);

    }
}