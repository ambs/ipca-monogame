using System.Collections.Generic;
using System.Linq;
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;


namespace IPCA.Monogame {

    public class KeyboardManager : GameComponent {

        /// <summary>
        /// Each key can have one of four different key states:
        /// <list type="bullet">
        /// <item><term>Down</term><description>when it is pressed for more than one frame;</description></item>
        /// <item><term>Up</term><description>when it is released for more than one frame;</description></item>
        /// <item><term>GoingDown</term><description>when it got pressed on this specific frame;</description></item>
        /// <item><term>GoingUp</term><description>when it got released on this specific frame;</description></item>
        /// </list>
        /// </summary>
        internal enum KeyState
        {
            Down,
            Up,
            GoingDown,
            GoingUp
        }

        private Game _game;
        Dictionary<Keys, KeyActions> _keyState;
        private static KeyboardManager _instance;
        /// <summary>
        /// Accessor to the singleton Instance.
        /// </summary>
        public static KeyboardManager Instance => _instance;

        /// <summary>
        /// The construtor for the Singleton. Should be called exactly once.
        /// It automatically adds itself to the Components array.
        /// </summary>
        /// <param name="game">Reference to the Game object</param>
        /// <param name="asComponent">When set to false, instance is not added to the components array</param>
        /// <exception cref="Exception">Raises exception when an instance is already created.</exception>
        public KeyboardManager(Game game, bool asComponent = true) : base(game)
        {
            _keyState = new Dictionary<Keys, KeyActions>();
            if (KeyboardManager._instance == null) {
                KeyboardManager._instance = this;
            } else {
                throw new System.Exception("Singleton with more than one instance for KeyboardManager");
            }

            if (asComponent)
            {
                game.Component.Add(this);
            }
        }
        


        internal class KeyActions {
            internal KeyState state;
            internal Dictionary <KeyState, List<Action>> actions;
            internal KeyActions (KeyState initialState) {
                state = initialState;
                actions = new Dictionary<KeyState, List<Action>>();
                foreach (KeyState ks in Enum.GetValues(typeof(KeyState)))
                {
                    actions[ks] = new List<Action>();
                }
            }
        }

       
      


        public override void Update(GameTime gameTime)
        {
            // Detetar mudanças de estado nas teclas... (todas)

            // Obter todas as teclas premidas neste momento.
            Keys[] pressedKeys = Keyboard.GetState().GetPressedKeys();

            foreach (Keys key in pressedKeys) {
                // A tecla é conhecida?
                if (_keyState.ContainsKey(key)) {
                    switch (_keyState[key].state)
                    {
                        case KeyState.Down:
                        case KeyState.GoingDown:
                            _keyState[key].state = KeyState.Down;
                            break;
                        case KeyState.Up:
                        case KeyState.GoingUp:
                            _keyState[key].state = KeyState.GoingDown;
                            break;
                    }
                }
                else {
                    _keyState[key] = new KeyActions(KeyState.GoingDown);
                }
            }

            // teclas que conhecemos mas que nao estao premidas
            foreach (Keys key in _keyState.Keys.ToArray())
            {
                if (!pressedKeys.Contains(key)) {
                    switch (_keyState[key].state)
                    {
                        case KeyState.GoingDown:
                        case KeyState.Down:
                            _keyState[key].state = KeyState.GoingUp;
                            break;
                        case KeyState.Up:
                        case KeyState.GoingUp:
                            _keyState[key].state = KeyState.Up;
                            break;
                    }
                }
            }

            // executar eventos
            foreach (Keys key in _keyState.Keys)
            {
                KeyState currentKeyState = _keyState[key].state;
                foreach (Action a in _keyState[key].actions[ currentKeyState ]) 
                {
                    a();
                }
            }

        }

        bool _IsKeyDown(Keys k) {
            return _keyState.ContainsKey(k) && (_keyState[k].state == KeyState.Down || _keyState[k].state == KeyState.GoingDown);
        }
        bool _IsKeyUp(Keys k) {
            return !_keyState.ContainsKey(k) || _keyState[k].state == KeyState.Up || _keyState[k].state == KeyState.GoingUp;
        }
        bool _IsKeyGoingDown(Keys k) {
            return _keyState.ContainsKey(k) && _keyState[k].state == KeyState.GoingDown;
        }
        bool _IsKeyGoingUp(Keys k) {
            return _keyState.ContainsKey(k) && _keyState[k].state == KeyState.GoingUp;
        }

        public static bool IsKeyDown(Keys k) { return KeyboardManager._instance._IsKeyDown(k); }
        public static bool IsKeyGoingDown(Keys k) { return KeyboardManager._instance._IsKeyGoingDown(k); }
        public static bool IsKeyUp(Keys k) { return KeyboardManager._instance._IsKeyUp(k); }
        public static bool IsKeyGoingUp(Keys k) { return KeyboardManager._instance._IsKeyGoingUp(k); }

        public static void SetDownAction(Keys k, Action a) { KeyboardManager._instance._SetAction(KeyState.Down, k, a); }
        public static void SetGoingDownAction(Keys k, Action a) { KeyboardManager._instance._SetAction(KeyState.GoingDown, k, a); }
        public static void SetUpAction(Keys k, Action a) { KeyboardManager._instance._SetAction(KeyState.Up, k, a); }
        public static void SetGoingUpAction(Keys k, Action a) { KeyboardManager._instance._SetAction(KeyState.GoingUp, k, a); }

        internal void _SetAction(KeyState ks, Keys key, Action a) 
        {
            if (!_keyState.ContainsKey(key)) {
                _keyState[key] = new KeyActions(KeyState.Up);
            }
            _keyState[key].actions[ks].Add(a);
        }            

    } 

}