
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PixelDrawing
{
    public class Pixel {
        public static Pixel instance;

        Texture2D pixel;
        SpriteBatch spriteBatch;

        public Pixel(Game1 game)
        {
            if (instance != null) 
                throw new Exception("Pixel singleton construtor called twice!");
                
            instance = this;
            pixel = new Texture2D(game.GraphicsDevice, 1, 1);
            pixel.SetData<Color>(new Color[]{ Color.White });

            spriteBatch = new SpriteBatch(game.GraphicsDevice);
        }

        public static void Draw(Vector2 position, Color color) {
            instance.spriteBatch.Begin();
            instance.DrawPixel(position, color);
            instance.spriteBatch.End();
        }

        public static void Draw(Rectangle rectangle, Color color) {
            instance.spriteBatch.Begin();
            instance.DrawPixel(rectangle, color);
            instance.spriteBatch.End();
        }

        public static void Draw(Vector2 o, Vector2 t, Color c) {
            instance.spriteBatch.Begin();
            instance.DrawLine(o.ToPoint(), t.ToPoint(), c);
            instance.spriteBatch.End();
        }

        public static void Draw(Vector2 center, float radius, Color c) {
            instance.spriteBatch.Begin();
            instance.DrawCircle(center, radius, c);
            instance.spriteBatch.End();
        }

        private void DrawCircle(Vector2 center, float radius, Color c)
        {
            float delta = 1f / radius; 
            for (float theta = 0 ; theta <= 2 * MathF.PI; theta += delta) {
                Vector2 p = (center + radius * new Vector2(MathF.Cos(theta), MathF.Sin(theta)));
                DrawPixel(p, c);
            }
        }

        private void DrawLine(Point origin, Point target, Color c) {
            if (origin.X == target.X) {
                // Vertical Line
                int yMin = Math.Min(origin.Y, target.Y);
                int length = Math.Abs(origin.Y - target.Y);
                DrawPixel(new Rectangle(origin.X, yMin, 1, length), c);
            }
            else if (origin.Y == target.Y) {
                // Horizontal Line
                int xMin = Math.Min(origin.X, target.X);
                int length = Math.Abs(origin.X - target.X);
                DrawPixel(new Rectangle(xMin, origin.Y, length, 1), c);
            }
            else {
                // Diagonals

                float m = ((float)(target.Y - origin.Y)/(target.X - origin.X));
                float b = origin.Y - m * origin.X;


                if (Math.Abs(origin.Y - target.Y) < Math.Abs(target.X - origin.X)) {
                    // Mostly horizontal
                    int x0 = Math.Min(origin.X, target.X);
                    int x1 = Math.Max(origin.X, target.X);
                    for (int x = x0; x <= x1; x++) {
                        int y = (int)(m * x + b);
                        DrawPixel(new Vector2(x,y), c);
                    }
                } else {
                    //Mostly Vertical
                    int y0 = Math.Min(origin.Y, target.Y);
                    int y1 = Math.Max(origin.Y, target.Y);
                    for (int y = y0; y <= y1; y++) {
                        int x = (int)(( y - b) / m);
                        DrawPixel(new Vector2(x,y), c);
                    }
                }
            }
        }
        private void DrawPixel(Vector2 position, Color color)
        {
            spriteBatch.Draw(pixel, position, color);
        }
          private void DrawPixel(Rectangle position, Color color)
        {
            spriteBatch.Draw(pixel, position, color);
        }
    }
}